<?php

namespace Chatter\Middleware;

class Logging {
    public function __invoke($request, $response, $next){
        //before route
        error_log($request->getMethod()."--".$request->getUri()."\r\n"    /*get the method (like post,get...) and the url of the method (like user,message..*/
        ,3,"log.txt");
        $response = $next($request, $response); //go to the next middleware. (if there is no middleware, go to the route)
        return $response;
        //after route
    }
}